package rpcxclient

import (
	"context"
	"fmt"
	"sync"
	"time"

	"github.com/smallnest/rpcx/client"
	"github.com/smallnest/rpcx/protocol"
	"github.com/smallnest/rpcx/share"

	"gitee.com/gomods/config"
	"gitee.com/gomods/logger"
)

var (
	sdaddrs            []string //注册中心地址
	defaultBasePath    string
	group              string
	templates          map[string]client.ServiceDiscovery
	appId              string
	appKey             string
	mu                 sync.RWMutex
	rpcxOptCallTimeout time.Duration //调用超时
	rpcxConnectTimeout time.Duration //连接超时
)

func init() {
	templates = make(map[string]client.ServiceDiscovery, 0)

	sdmap := config.GetConfArrayMap("Registration")
	sdaddrs = sdmap["addrs"]
	if callTimeout := config.GetConf("Registration", "rpcCallTimeout"); callTimeout != "" {
		if d, err := time.ParseDuration(callTimeout); err == nil {
			rpcxOptCallTimeout = d
		}
	}
	rpcxConnectTimeout = time.Second //default 1s
	if connectTimeout := config.GetConf("Registration", "rpcConnectTimeout"); connectTimeout != "" {
		if d, err := time.ParseDuration(connectTimeout); err == nil {
			rpcxConnectTimeout = d
		}
	}

	defaultBasePath = config.GetConf("Registration", "basePath")
	group = config.GetConf("Registration", "group")
	if len(sdaddrs) <= 0 {
		sdmap = config.GetConfArrayMap("Registry")
		sdaddrs = sdmap["addrs"]
		defaultBasePath = config.GetConf("Registry", "basePath")
		group = config.GetConf("Registry", "group")
	}
	rpcAuth := config.GetConfStringMap("RpcxAuth")
	appId = rpcAuth["appId"]
	appKey = rpcAuth["appKey"]
}

func GetSdAddrs() []string {
	return sdaddrs
}

func GetServiceBasePath() string {
	return defaultBasePath
}

func GetFailMode() client.FailMode {
	return client.Failover
}

//func GetRegCenter() string {
//	return regcenter
//}

func GetSelectMode() client.SelectMode {
	return client.WeightedRoundRobin
}

func GetClientOption() client.Option {
	option := client.Option{
		Retries:           2,
		RPCPath:           share.DefaultRPCPath,
		ConnectTimeout:    rpcxConnectTimeout,
		SerializeType:     protocol.MsgPack,
		CompressType:      protocol.None,
		BackupLatency:     10 * time.Millisecond,
		Heartbeat:         true,
		HeartbeatInterval: 1 * time.Second,
		Group:             group,
		//WriteTimeout:      2 * time.Second,
		//ReadTimeout:       2 * time.Second,
	}

	//if failed 5 times, return error immediately, and will try to connect after 10 seconds
	option.GenBreaker = func() client.Breaker {
		return client.NewConsecCircuitBreaker(5, 10*time.Second)
	}
	return option
}

func getClientDiscovery(basePath, servicePath string) (discovery client.ServiceDiscovery) {
	mu.Lock()
	defer mu.Unlock()

	ctx := context.Background()
	tag := "rpcxclient.init.GetClientDiscovery"

	if basePath == "" {
		basePath = defaultBasePath
	}

	defer func() {
		if e := recover(); e != nil {
			logger.Ex(ctx, tag, "初始化服务发现对象失败", "error", fmt.Sprintf("%v", e))
		}
	}()

	var err error
	if template, ok := templates[basePath]; !ok {
		template, err = initClientDiscovery(basePath)
		if err != nil {
			logger.Ex(ctx, tag, "初始化服务发现对象失败", "error", err.Error())
			return
		}

		templates[basePath] = template
		discovery, err = template.Clone(servicePath)
	} else {
		discovery, err = template.Clone(servicePath)
	}

	if err != nil {
		logger.Ex(ctx, tag, "初始化服务发现对象失败", "error", err.Error())
	}

	return discovery
}

func GetAppId() string {
	return appId
}
