package rpcxclient

import (
	"crypto/md5"
	"encoding/hex"
	"strconv"
	"time"
)

func genRpcAuth() (string, string) {
	now := strconv.Itoa(int(time.Now().Unix()))
	md5Ctx := md5.New()
	md5Ctx.Write([]byte(appId + "&" + now + appKey))
	cipherStr := md5Ctx.Sum(nil)
	signstr := hex.EncodeToString(cipherStr)
	return now, signstr
}

func genRpcAuthCtx(appId, appKey string) (string, string) {
	now := strconv.Itoa(int(time.Now().Unix()))
	md5Ctx := md5.New()
	md5Ctx.Write([]byte(appId + "&" + now + appKey))
	cipherStr := md5Ctx.Sum(nil)
	signstr := hex.EncodeToString(cipherStr)
	return now, signstr
}
