//go:build etcd
// +build etcd

package rpcxclient

import (
	"github.com/rpcxio/rpcx-etcd/client"
	eClient "github.com/rpcxio/rpcx-etcd/client"
)

func initClientDiscovery(basePath string) (client.ServiceDiscovery, error) {
	return eClient.NewEtcdV3DiscoveryTemplate(basePath, GetSdAddrs(), true, nil)
}
