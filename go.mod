module gitee.com/gomods/rpcxclient

go 1.15

require (
	gitee.com/gomods/config v0.0.1
	gitee.com/gomods/logger v0.0.1
	github.com/rpcxio/rpcx-etcd v0.3.2
	github.com/rpcxio/rpcx-zookeeper v0.0.0-20220730061732-d20531677676
	github.com/smallnest/rpcx v1.7.11
	github.com/smartystreets/goconvey v1.7.2 // indirect
	github.com/spf13/cast v1.3.1
	gopkg.in/natefinch/lumberjack.v2 v2.0.0 // indirect
)
